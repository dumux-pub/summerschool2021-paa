#!/usr/bin/env python3
#
# This installs the module SummerSchool2021-PAA and its dependencies.
# The exact revisions used are listed in the table below.
# However, note that this script may also apply further patches.
# If so, all patches are required to be the current folder, or,
# in the one that you specified as argument to this script.
#
#
# |      module name       |      branch name      |                 commit sha                 |         commit date         |
# |------------------------|-----------------------|--------------------------------------------|-----------------------------|
# |      dune-common       |  origin/releases/2.7  |  aa689abba532f40db8f5663fa379ea77211c1953  |  2020-11-10 13:36:21 +0000  |
# |  dune-localfunctions   |  origin/releases/2.7  |  68f1bcf32d9068c258707d241624a08b771b6fde  |  2020-11-26 23:45:36 +0000  |
# |     dune-geometry      |  origin/releases/2.7  |  9d56be3e286bc761dd5d453332a8d793eff00cbe  |  2020-11-26 23:26:48 +0000  |
# |       dune-istl        |  origin/releases/2.7  |  761b28aa1deaa786ec55584ace99667545f1b493  |  2020-11-26 23:29:21 +0000  |
# |       dune-grid        |  origin/releases/2.7  |  b7741c6599528bc42017e25f70eb6dd3b5780277  |  2020-11-26 23:30:08 +0000  |
# |      dune-subgrid      |     origin/master     |  c0f298d09cd66d5647951d6797cdb1524683abae  |  2021-04-20 06:54:40 +0000  |
# |     dune-foamgrid      |     origin/master     |  d49187be4940227c945ced02f8457ccc9d47536a  |  2020-01-06 15:36:03 +0000  |
# |      dumux-course      |     origin/master     |  0a53b97b21a725f63cc06502dbd03f814af17ee5  |  2021-05-18 21:30:51 +0000  |
# |         dumux          |     origin/master     |  4455a367f42bc2ea985045737073c4f3e8c3f76d  |  2021-08-03 07:01:17 +0000  |
# |  summerschool2021-paa  |     origin/master     |  dc0bbbdc81e701c5f96238e247874655713dfab7  |  2021-08-03 09:52:49 +0200  |

import os
import sys
import subprocess

top = "DUMUX_summerschool"
os.makedirs(top, exist_ok=True)

def runFromSubFolder(cmd, subFolder):
    folder = os.path.join(top, subFolder)
    try:
        subprocess.run(cmd, cwd=folder, check=True)
    except Exception as e:
        cmdString = ' '.join(cmd)
        sys.exit(
            "Error when calling:\n{}\n-> folder: {}\n-> error: {}"
            .format(cmdString, folder, str(e))
        )

def installModule(subFolder, url, branch):
    targetFolder = url.rstrip(".git").split("/")[-1]
    if not os.path.exists(targetFolder):
        runFromSubFolder(['git', 'clone', url, targetFolder], '.')
        runFromSubFolder(['git', 'checkout', branch], subFolder)
    else:
        print(f'Skip cloning {url} since target folder "{targetFolder}" already exists.')


def applyPatch(subFolder, patch):
    sfPath = os.path.join(top, subFolder)
    patchPath = os.path.join(sfPath, 'tmp.patch')
    with open(patchPath, 'w') as patchFile:
        patchFile.write(patch)
    runFromSubFolder(['git', 'apply', 'tmp.patch'], subFolder)
    os.remove(patchPath)

print("Installing summerschool2021-paa")
installModule("summerschool2021-paa", "https://git.iws.uni-stuttgart.de/dumux-pub/summerschool2021-paa.git", "master")

print("Installing dumux")
installModule("dumux", "https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git", "master")

print("Installing dumux-course")
installModule("dumux-course", "https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-course.git", "master")

print("Installing dune-subgrid")
installModule("dune-subgrid", "https://git.imp.fu-berlin.de/agnumpde/dune-subgrid.git", "master")

print("Installing dune-localfunctions")
installModule("dune-localfunctions", "https://gitlab.dune-project.org/core/dune-localfunctions.git", "releases/2.7")

print("Installing dune-foamgrid")
installModule("dune-foamgrid", "https://gitlab.dune-project.org/extensions/dune-foamgrid.git", "master")

print("Installing dune-istl")
installModule("dune-istl", "https://gitlab.dune-project.org/core/dune-istl.git", "releases/2.7")

print("Installing dune-geometry")
installModule("dune-geometry", "https://gitlab.dune-project.org/core/dune-geometry.git", "releases/2.7")

print("Installing dune-grid")
installModule("dune-grid", "https://gitlab.dune-project.org/core/dune-grid.git", "releases/2.7")

print("Installing dune-common")
installModule("dune-common", "https://gitlab.dune-project.org/core/dune-common.git", "releases/2.7")

print("Configuring project")
runFromSubFolder(
    ['./dune-common/bin/dunecontrol', '--opts=summerschool2021-paa/cmake.opts', 'all'],
    '.'
)
