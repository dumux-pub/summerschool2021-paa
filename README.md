## SFB-1313 Summer School 2021: Porescale evaluation of REV Scale Coupling Conditions

This repository will house all of the dumux based simulations used during the
2021 SFB1313 Summer School. Simulations are found within the `appl` or applications
folder.

You can configure the module just like any other DUNE module
by using `dunecontrol`. For building and running the executables,
please go to the build folders corresponding to the simulation sources listed above.

