add_input_file_links()

dumux_add_test(NAME test_coupled_channel
              SOURCES main.cc
              LABELS REV coupled
              CMAKE_GUARD HAVE_UMFPACK
              COMMAND ./test_coupled_channel
              CMD_ARGS params.input)

