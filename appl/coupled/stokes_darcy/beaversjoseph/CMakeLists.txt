add_input_file_links()

dumux_add_test(NAME test_coupled_bjs
              SOURCES main.cc
              LABELS REV coupled
              CMAKE_GUARD HAVE_UMFPACK
              COMMAND ./test_coupled_bjs
              CMD_ARGS params.input)

