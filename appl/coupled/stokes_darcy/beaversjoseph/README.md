__Two-domain Coupled model__:
=========================
### Freeflow model
- __Navier Stokes Model__: Navier Stokes model used to determine the flow field. A pressure gradient is applied from left to right.
- __Staggered Grid__: Scalar quantities (Pressure) are cellcentered and evaluated with TPFA, Vector (Velocity) quantities are evaluated at the cell faces.

### Porous Media flow model
- __Darcy's law__: Uses darcy's law to evaluate momentum transport within the porous medium. A pressure gradient is applied from left to right.
- __Cell Centered Finite Volumes__: Scalar quantities (Pressure) are cellcentered and evaluated with TPFA

### Geometry
The geometry uses two domains equal in size, where the lower domain is porous, and the higher is free.

<figure>
    <center>
        <img src="figs/REV_BJS_Solution.png" alt="REV setup" width="80%"/>
        <figcaption> <b> Fig.1 </b> - Channel Configuration with a coupled model </figcaption>
    </center>
</figure>
