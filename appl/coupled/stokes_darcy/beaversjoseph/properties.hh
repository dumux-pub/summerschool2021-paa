// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief the stokes and darcy properties files for the coupled channel simulation
 */

#ifndef DUMUX_COUPLED_BJS_PROPERTIES_HH
#define DUMUX_COUPLED_BJS_PROPERTIES_HH

// Both Domains
#include <dune/grid/yaspgrid.hh>
#include <dune/common/hybridutilities.hh>

#include <dumux/multidomain/staggeredtraits.hh>
#include <dumux/multidomain/boundary/stokesdarcy/couplingmanager.hh>

#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/components/constant.hh>

// Free-flow domain
#include <dumux/discretization/staggered/freeflow/properties.hh>
#include <dumux/freeflow/navierstokes/model.hh>
#include <dumux/freeflow/navierstokes/boundarytypes.hh>

// Porous medium flow domain
#include <dumux/discretization/cctpfa.hh>
#include <dumux/discretization/box.hh>
#include <dumux/porousmediumflow/1p/model.hh>

#include "spatialparams.hh"
#include "problem_stokes.hh"
#include "problem_darcy.hh"

namespace Dumux::Properties {

namespace TTag {
struct NavierStokesDomain { using InheritsFrom = std::tuple<NavierStokes, StaggeredFreeFlowModel>; };
struct DarcyDomain { using InheritsFrom = std::tuple<OneP, BoxModel>; };
} // end namespace TTag

// The coupling managers
template<class TypeTag>
struct CouplingManager<TypeTag, TTag::NavierStokesDomain>
{
    using Traits = StaggeredMultiDomainTraits<TypeTag, TypeTag, Properties::TTag::DarcyDomain >;
    using type = Dumux::StokesDarcyCouplingManager<Traits>;
};

// Set the problem properties
template<class TypeTag>
struct Problem<TypeTag, TTag::NavierStokesDomain> { using type = Dumux::StokesSubProblem<TypeTag>; };

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::NavierStokesDomain>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = FluidSystems::OnePLiquid<Scalar, Components::Constant<1, Scalar>>;
};

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::NavierStokesDomain>
{
    static constexpr auto dim = 2;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using TensorGrid = Dune::YaspGrid<2, Dune::TensorProductCoordinates<Scalar, dim> >;
    using HostGrid = TensorGrid;
    using type = Dune::SubGrid<dim, HostGrid>;
};

// Set the upwind scheme order for the velocity calculation
template<class TypeTag>
struct UpwindSchemeOrder<TypeTag, TTag::NavierStokesDomain> { static constexpr int value = 1; };

template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::NavierStokesDomain> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::NavierStokesDomain> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::NavierStokesDomain> { static constexpr bool value = true; };

// The coupling managers
template<class TypeTag>
struct CouplingManager<TypeTag, TTag::DarcyDomain>
{
    using Traits = StaggeredMultiDomainTraits<Properties::TTag::NavierStokesDomain, Properties::TTag::NavierStokesDomain, TypeTag>;
    using type = Dumux::StokesDarcyCouplingManager<Traits>;
};

// Set the problem properties
template<class TypeTag>
struct Problem<TypeTag, TTag::DarcyDomain> { using type = Dumux::DarcySubProblem<TypeTag>; };

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::DarcyDomain>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = FluidSystems::OnePLiquid<Scalar, Components::Constant<1, Scalar>>;
};

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::DarcyDomain>
{
    static constexpr auto dim = 2;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using TensorGrid = Dune::YaspGrid<2, Dune::TensorProductCoordinates<Scalar, dim> >;
    using HostGrid = TensorGrid;
    using type = Dune::SubGrid<dim, HostGrid>;
};

template<class TypeTag>
struct SpatialParams<TypeTag, TTag::DarcyDomain>
{
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = DarcySpatialParams<GridGeometry, Scalar>;
};

}
#endif
