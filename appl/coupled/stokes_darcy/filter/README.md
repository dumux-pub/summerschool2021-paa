__Two-domain Coupled model__:
=========================
### Freeflow model
- __Navier Stokes Model__: Navier Stokes model used to determine the flow field
- __Staggered Grid__: Scalar quantities (Pressure) are cellcentered and evaluated with TPFA, Vector (Velocity) quantities are evaluated at the cell faces.

### Porous Media flow model
- __Darcy's law__: Uses darcy's law to evaluate momentum transport within the porous medium
- __Cell Centered Finite Volumes__: Scalar quantities (Pressure) are cellcentered and evaluated with TPFA


### Geometry
The geometry is the same as the Pore-Scale Filter simualtion, except that the porous media region is replaced with a separate porous medium domain.

<figure>
    <center>
        <img src="figs/Filter_REV.png" alt="REV setup" width="80%"/>
        <figcaption> <b> Fig.1 </b> - Filter Configuration with a coupled model </figcaption>
    </center>
</figure>
