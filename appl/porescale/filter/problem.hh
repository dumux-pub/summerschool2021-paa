// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

/*!
 * \file
 * \ingroup Freeflow Porous Media Flow
 * \ingroup Channel Pore Structure
 * \brief The problem file for the evaluation of a pore geometry adjacent to a freeflow with RANS models
 */

#ifndef DUMUX_CHANNEL_PORE_STRUCTURE_PROBLEM_HH
#define DUMUX_CHANNEL_PORE_STRUCTURE_PROBLEM_HH

#include <dumux/common/properties.hh>
#include <dumux/common/numeqvector.hh>
#include <dumux/freeflow/navierstokes/problem.hh>

namespace Dumux {

template <class TypeTag>
class PoreStructurePMProblem : public NavierStokesProblem<TypeTag>
{
    using ParentType = NavierStokesProblem<TypeTag>;

    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using FluidState = GetPropType<TypeTag, Properties::FluidState>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;

    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
    using BoundaryTypes = Dumux::NavierStokesBoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using Element = typename GridGeometry::GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;

    static constexpr auto dimWorld = GridGeometry::GridView::dimensionworld;
    using VelocityVector = Dune::FieldVector<Scalar, dimWorld>;

public:
    PoreStructurePMProblem(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry), eps_(1e-8)
    {
        inletVelocity_ = getParam<Scalar>("Problem.InletVelocity");
        porousMediaBoxMax_ = getParam<GlobalPosition>("PoreStructure.PorousMediaBoxMin");
        porousMediaBoxMax_ = getParam<GlobalPosition>("PoreStructure.PorousMediaBoxMax");
        problemName_  =  getParam<std::string>("Vtk.OutputName") + "_" + getParam<std::string>("PoreStructure.PoreGeometryType");
    }

   /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief Returns the temperature [K] within the domain for the isothermal model.
     */
    Scalar temperature() const
    { return 298.15; }

    /*!
     * \brief The problem name.
     */
    const std::string& name() const
    { return problemName_; }

    // \}

   /*!
     * \name Boundary conditions
     */
    // \{

   /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary control volume.
     *
     * \param globalPos The position of the center of the finite volume
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;

        // common boundary types for all turbulence models
        if (isOutlet_(globalPos))
            values.setDirichlet(Indices::pressureIdx);
        else if (isPorousMediaWall_(globalPos))
            values.setAllSymmetry();
        else
        {
            // Set inlet Velocity and wall velocity
            values.setDirichlet(Indices::velocityXIdx);
            values.setDirichlet(Indices::velocityYIdx);
        }

        return values;
    }
    /*!
      * \brief Evaluates the boundary conditions for a Dirichlet control volume.
      *
      * \param globalPos The center of the finite volume which ought to be set.
      */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values(0.0);
        if(isInlet_(globalPos))
            values[Indices::velocityXIdx] = parabolicProfile_(globalPos[1]);

        return values;
    }

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param globalPos The global position
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    { return PrimaryVariables(0.0); }

    // \}

    // Return cellCenteredVelocities
    std::vector<VelocityVector> cellCenteredVelocity(const SolutionVector& curSol) const
    {
        std::vector<VelocityVector> velocity;
        velocity.resize(this->gridGeometry().elementMapper().size(), VelocityVector(0.0));

        // calculate cell-center-averaged velocities
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);
            unsigned int elementIdx = this->gridGeometry().elementMapper().index(element);

            // calculate velocities
            VelocityVector velocityTemp(0.0);
            for (auto&& scvf : scvfs(fvGeometry))
            {
                const int dofIdxFace = scvf.dofIndex();
                const auto numericalSolutionFace = curSol[GridGeometry::faceIdx()][dofIdxFace][Indices::velocity(scvf.directionIndex())];
                velocityTemp[scvf.directionIndex()] += numericalSolutionFace;
            }
            for (unsigned int dimIdx = 0; dimIdx < dimWorld; ++dimIdx)
                velocity[elementIdx][dimIdx] = velocityTemp[dimIdx] * 0.5; // faces are equidistant to cell center
        }
        return velocity;
    }

private:
    bool isInlet_(const GlobalPosition& globalPos) const
    { return globalPos[0] < this->gridGeometry().bBoxMin()[0] + eps_; }

    bool isOutlet_(const GlobalPosition& globalPos) const
    { return globalPos[0] > this->gridGeometry().bBoxMax()[0] - eps_; }

    bool isOnUpperWall_(const GlobalPosition& globalPos) const
    { return globalPos[1] > this->gridGeometry().bBoxMax()[1] - eps_; }

    bool isOnLowerWall_(const GlobalPosition& globalPos) const
    { return (globalPos[1] < porousMediaBoxMax_[1] + eps_);}

    bool isInPorousMedium_(const GlobalPosition& globalPos) const
    { return ( ( (globalPos[0] > porousMediaBoxMin_[0] + eps_)
              && (globalPos[0] < porousMediaBoxMax_[0] - eps_) )
            && ( (globalPos[1] > porousMediaBoxMin_[1] + eps_)
              && (globalPos[1] < porousMediaBoxMax_[1] - eps_) ) ); }

    bool isPorousMediaWall_(const GlobalPosition& globalPos) const
    { return ( ( (globalPos[1] > porousMediaBoxMin_[1] + eps_)
              && (globalPos[1] < porousMediaBoxMax_[1] - eps_) )
            && ( (globalPos[0] < porousMediaBoxMin_[0] + eps_)
              || (globalPos[0] > porousMediaBoxMax_[0] - eps_) ) ); }

    Scalar parabolicProfile_(const Scalar y) const
    {
        const Scalar yMin = porousMediaBoxMax_[1];
        const Scalar yMax = this->gridGeometry().bBoxMax()[1];
        const Scalar vMax = inletVelocity_;
        return  vMax * (y - yMin)*(yMax - y) / (0.25*(yMax - yMin)*(yMax - yMin));
    }

    // the height of the free-flow domain
    const Scalar height_() const
    { return this->gridGeometry().bBoxMax()[1] - porousMediaBoxMax_[1]; }

    GlobalPosition porousMediaBoxMin_;
    GlobalPosition porousMediaBoxMax_;
    Scalar inletDiameter_;
    Scalar outletDiameter_;

    Scalar inletVelocity_;

    Scalar eps_;
    std::string problemName_;
};

} // end namespace Dumux

#endif
