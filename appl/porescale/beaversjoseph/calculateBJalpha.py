import numpy as np
import subprocess

############################################
############### Paramters ##################
############################################

l = 0.05
porosityList = [0.3972, 0.547552, 0.898896]
IntrinsicPermeabilityList = np.array([0.000536583, 0.00292123, 0.0397444])*l*l
DarcyVelocityWholeDomain = 0.0105814

############################################
#### Simulation and Data extract ###########
############################################

for porosity, IntrinsicPermeability in zip(porosityList, IntrinsicPermeabilityList):
    subprocess.call(['./test_porescale_bjs', 'params.input',
                     '-Problem.Name', 'PoreScale_BJS'+str(porosity),
                     '-Vtk.OutputName', 'PoreScale_BJS'+str(porosity),
                     '-PoreStructure.Porosity', str(porosity)])

    subprocess.call(['/Applications/ParaView-5.9.0.app/Contents/bin/pvpython', 'extractlinedata.py','-f', 'PoreScale_BJS'+str(porosity)+'_Top-00001.vtu', '-p1','0.025', '0.001', '0.0','-p2','0.025', '0.999', '0.0','-r', '499'])
    subprocess.call(['/Applications/ParaView-5.9.0.app/Contents/bin/pvpython', 'extractlinedata.py','-f', 'AveragedFields_PoreScale_BJS'+str(porosity)+'_Top-00000.vtu', '-p1','0.025', '0.001', '0.0','-p2','0.025', '0.999', '0.0','-r','499'])

    ############################################
    ########## Data handling ###################
    ############################################
    avgData = np.genfromtxt('AveragedFields_PoreScale_BJS'+str(porosity)+'_Top-00000.csv', skip_header=True, delimiter=',')
    poreData = np.genfromtxt('PoreScale_BJS'+str(porosity)+'_Top-00001.csv', skip_header=True, delimiter=',')

    vxAverage = avgData[:, 3]
    vyAverage = avgData[:, 4]
    avgYList = avgData[:, -2]

    vxPore = poreData[:, 3]
    vyPore = poreData[:, 4]
    poreYList = poreData[:, -2]

    # calculate the velocity magnitude
    avgV = []
    for vx, vy in zip(vxAverage, vyAverage):
        avgV.append(np.sqrt(vx*vx + vy*vy))

    # print(avgV)

    poreV = []
    for vx, vy in zip(vxPore, vyPore):
        Vmag = np.sqrt(vx*vx + vy*vy)
        poreV.append(Vmag)



    # add the gradient field evaluated from the LA
    gradientLA = []
    for i in range((len(avgV)-1)):
        gd = (avgV[i+1] - avgV[i])/0.001
        gradientLA.append(gd)

    # slip velocity
    vGradient = np.max(gradientLA)
    index = np.argmax(gradientLA)
    print("The index is : ", index)
    v_s = avgV[index] - DarcyVelocityWholeDomain
    # alpha at interface
    alphaAtInterface = np.sqrt(IntrinsicPermeability)*v_s/vGradient
    # print("The slip velocity is :", v_s)
    print("The alpha value at interface is : ", alphaAtInterface)
    # print("The max gradient is : ", vGradient)
    print("The Reynolds number estimation is :", np.max(avgV)*0.75)


    # print(gradientLA)

    alphaList = []
    for i in range(len(avgV)-1):
        alpha = np.sqrt(IntrinsicPermeability)*(avgV[i] - DarcyVelocityWholeDomain)/gradientLA[i]
        # print(gradient[i])
        alphaList.append(alpha)

    ############################################
    ########## Plot ############################
    ############################################

    import matplotlib.pyplot as plt

    plt.plot(poreYList, poreV, label="Pore velocity")
    # plt.plot(poreYList, gradient, label="Pore gradient")
    # plt.plot(poreYList[495:510], alphaList[495:510])

    plt.legend()
    # plt.show()
    plt.savefig('plot'+str(porosity)+'.pdf')

    dataset_uPore_y = np.column_stack((poreYList, poreV))
    dataset_uLA_y = np.column_stack((avgYList[:-1], avgV[:-1], gradientLA, alphaList))

    np.savetxt("u_over_y"+str(porosity)+".csv", dataset_uPore_y, fmt=['%.11f', '%.11f'])
    np.savetxt("ud_over_y"+str(porosity)+".csv", dataset_uLA_y, fmt=['%.11f', '%.11f', '%.11f', '%.11f'])
