__Pore Scale Channel Simulation__:
===================================

### Single Model Concept
- __Navier Stokes Model__: Navier Stokes model used to determine the flow field. A pressure gradient is imposed from left to right.
- __Staggered Grid__: Scalar quantities (Pressure) are cellcentered and evaluated with TPFA, Vector (Velocity) quantities are evaluated at the cell faces.

### Geometry
- These tests are based on the concept of a channel flow with a box of porous material beneath.
- The pore geometry within the porous box can be altered with the parameters in the `[PoreStructure]' parameter group.
- The configuration of the pores can be selected to be `Uniform`, `Staggered`, `Random`, or `Random-ConstantPorosity` for Blocks, or 'Centered" or 'Top' for round inclusions within the `PoreGeometryType` parameter.
- The number of pores and the approximate porosity can be determined with the `NumPores` and the `Porosity` parameters

For example, the Baseline Case with `Centered` configuration at .50 porosity and with 10 pores (1x10 , would look like the following:
<figure>
    <center>
        <img src="figs/Porescale_BJS_solution_scaled.png" alt="Pore scale setup" width="3%"/>
        <figcaption> <b> Fig.1 </b> - 1x10 Staggered Configuration at 0.5 porosity </figcaption>
    </center>
</figure>

| porosity | 0.2976	|0.3472	|0.3968| 0.448|	0.4976|	0.5504|	0.5984 |
| permeability body conforming mesh | 1.71894E-007 | 5.80807E-007 |	1.3527E-006	|0.000002662| 4.58242E-006| 7.55053E-006| 0.000011327 |
| permeability stair case approximation  |  9.75896E-008 |	4.57133E-007	| 1.1263E-006 |	0.000002351	| 4.23256E-006 |6.85576E-006 |	1.05104E-005 |
| darcy velocity | ? | ? |
| interface position Elissa | 
| interface positon Ugis|
| interface position Paula|
| interface position top of solid inclusions |


| header | header |
| ------ | ------ |
| porosity | cell |
| permeability | cell |



### Averaging
- After the flow problem is solved, the solution within the porous media region is volume averaged. The number of volumes to average over is determined with `NumVolumes`.
- Average Velocity and Pressure values, as well as the Porosity are calcualted and output to an averaged grid file.

For example, The volume averaging over 100 volumes of the above mentioned staggered configuration would look like the following:
<figure>
    <center>
        <img src="figs/VolumeAveraged_Porescale_BJS_solution.png" alt="Volume Average setup" width="80%"/>
        <figcaption> <b> Fig.2 </b> - 10x10 Volume Averaging </figcaption>
    </center>
</figure>
