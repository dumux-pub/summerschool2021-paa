__Pore Scale Channel Simulation__:
===================================

### Single Model Concept
- __Navier Stokes Model__: Navier Stokes model used to determine the flow field
- __Staggered Grid__: Scalar quantities (Pressure) are cellcentered and evaluated with TPFA, Vector (Velocity) quantities are evaluated at the cell faces.

### Geometry
- These tests are based on the concept of a channel flow with a box of porous material beneath.
- The pore geometry within the porous box can be altered with the parameters in the `[PoreStructure]' parameter group.
- The configuration of the pores can be selected to be `Uniform`, `Staggered`, `Random`, or `Random-ConstantPorosity` within the `PoreGeometryType` parameter
- The number of pores and the approximate porosity can be determined with the `NumPores` and the `Porosity` parameters

For example, the `Staggered` configuration at .50 porosity and with 200 pores (20x10), would look like the following:
<figure>
    <center>
        <img src="figs/staggered_porescale.png" alt="Pore scale setup" width="80%"/>
        <figcaption> <b> Fig.1 </b> - 20x10 Staggered Configuration at 0.5 porosity </figcaption>
    </center>
</figure>


### Averaging
- After the flow problem is solved, the solution within the porous media region is volume averaged. The number of volumes to average over is determined with `NumVolumes`.
- Average Velocity and Pressure values, as well as the Porosity are calcualted and output to an averaged grid file.

For example, The volume averaging over 200 volumes of the above mentioned staggered configuration would look like the following:
<figure>
    <center>
        <img src="figs/staggered_average.png" alt="Volume Average setup" width="80%"/>
        <figcaption> <b> Fig.2 </b> - 20x10 Volume Averaging </figcaption>
    </center>
</figure>
