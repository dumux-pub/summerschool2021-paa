// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

/*!
 * \file
 * \ingroup Freeflow Porous Media Flow
 * \ingroup Channel Pore Structure
 * \brief A test for the evaluation of a pore geometry adjacent to a freeflow
 */

#include <config.h>
#include <ctime>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/grid/io/file/dgfparser/dgfexception.hh>
#include <dune/grid/io/file/gmshwriter.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/istl/io.hh>

#include <dumux/assembly/staggeredfvassembler.hh>
#include <dumux/assembly/diffmethod.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/properties.hh>
#include <dumux/io/grid/gridmanager.hh>
#include <dumux/io/staggeredvtkoutputmodule.hh>
#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/nonlinear/newtonsolver.hh>
#include <dumux/discretization/method.hh>

#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/discretization/cellcentered/tpfa/fvgridgeometry.hh>

#include <dumux/poregeometries.hh>
#include "properties.hh"

int main(int argc, char** argv)
{
    ////////////////////////////////////
    // Initialize and Call Parameters //
    ////////////////////////////////////

    using namespace Dumux;
    // define the type tag for this problem
    using TypeTag = Properties::TTag::PoreScaleModel;

    // Initialize MPI, finalize is done automatically on exit. Print dumux start message
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv);





    ////////////////////////////////////////////////
    // Step 1: Build the Pore Scale Grid Geometry //
    ////////////////////////////////////////////////

    std::cout << "\nSTEP 1: Construct a pore scale geometry to simulate on. \n";
    // The hostgrid
    constexpr int dim = 2;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using GlobalPosition = Dune::FieldVector<Scalar, dim>;
    using DimensionPair = Dune::FieldVector<unsigned int, dim>;
    using HostGrid = Dune::YaspGrid<2, Dune::TensorProductCoordinates<Scalar, dim> >;
    using SubGrid = Dune::SubGrid<dim, HostGrid>;
    using HostGridManager = GridManager<HostGrid>;
    HostGridManager hostGridManager;
    hostGridManager.init();
    auto& hostGrid = hostGridManager.grid();


    bool buildNewPoreGeometry = getParam<bool>("PoreStructure.BuildNewPoreGeometry", true);

    DimensionPair numPores = getParam<DimensionPair>("PoreStructure.NumPores");
    std::pair<GlobalPosition, GlobalPosition> porousMediaBox = {getParam<GlobalPosition>("PoreStructure.PorousMediaBoxMin"),
                                                                getParam<GlobalPosition>("PoreStructure.PorousMediaBoxMax")};
    Scalar porosity = getParam<Scalar>("PoreStructure.Porosity");
    std::vector<PoreGeometries::PoreBlock<dim>> poreBlockList;

    std::string poreGeometryType = getParam<std::string>("PoreStructure.PoreGeometryType");
    std::string poreShape = getParam<std::string>("PoreStructure.PoreShape");
    if (buildNewPoreGeometry)
    {
        std::cout << "The Pore geometry description will be developed according to the parameters provided. \n";

        PoreGeometries::buildPoreGeometry(poreShape, poreGeometryType, poreBlockList, porosity, numPores, porousMediaBox);

        bool writeOutPoreGeometryFile = getParam<bool>("PoreStructure.WriteOutPoreGeometryFile", false);
        if (writeOutPoreGeometryFile)
        {
            std::string outputPoreGeometryFileName = "PoreGeometry_" + poreGeometryType;
            std::cout << "The Grid geometry will be written out to a file named " << outputPoreGeometryFileName << ".csv \n";
            PoreGeometries::writeOutPoreGeometryFileToCSV(poreBlockList, outputPoreGeometryFileName);
        }
    }
    else
    {
        std::string gridInputFileName = getParam<std::string>("PoreStructure.GridInputFileName");
        std::cout << "The Pore geometry description will be read in from the provided file named: " << gridInputFileName << " \n";
        PoreGeometries::readInPoreGeometry(poreBlockList, gridInputFileName);
    }

    // The subgrid selector
    auto gridSelector = [&](const auto& element)
    {
        double eps = 1e-12;
        GlobalPosition globalPos = element.geometry().center();

        if (globalPos[1] > porousMediaBox.second[1] + eps)
            return true;
        else if ((globalPos[0] < porousMediaBox.first[0] + eps) || (globalPos[0] > porousMediaBox.second[0]))
            return false;
        else
        {
            for (int i = 0; i < poreBlockList.size(); i++)
            {
                if (poreShape == "Block")
                {
                    if ( (globalPos[0] > poreBlockList[i].poreBlockMin[0] + eps && globalPos[0] < poreBlockList[i].poreBlockMax[0] + eps)
                      && (globalPos[1] > poreBlockList[i].poreBlockMin[1] + eps && globalPos[1] < poreBlockList[i].poreBlockMax[1] + eps))
                        return false;
                }
                else if (poreShape == "Circle")
                {
                    if ( std::hypot(globalPos[0] - poreBlockList[i].centerPoint[0], globalPos[1] - poreBlockList[i].centerPoint[1]) < poreBlockList[i].radius )
                        return false;
                }
                else
                    DUNE_THROW(Dune::IOError, "The requested Pore Shape is not implemented."
                                           << " The options for Solid Inclusions are Block and Circle. \n");
            }
        }

        return true;
    };

    Dumux::GridManager<SubGrid> subGridManager;
    subGridManager.init(hostGrid, gridSelector, "PoreGeometry_Filter");

    // we compute on the leaf grid view
    const auto& poreScaleGridView = subGridManager.grid().leafGridView();

    // create the finite volume grid geometry
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    auto poreScaleGridGeometry = std::make_shared<GridGeometry>(poreScaleGridView);

    Dune::GmshWriter<std::decay_t<decltype(poreScaleGridView)>> gmshWriter(poreScaleGridView);
    gmshWriter.setPrecision(10);
    gmshWriter.write("porescalegrid.msh");
    std::cout << "Pore scale geometry is created. \n";








    //////////////////////////////////////////////////////
    // Step 2: Build the Volume Averaging Grid Geometry //
    //////////////////////////////////////////////////////

    // Build a new grid and prepare for volume averaging
    std::cout << "\nSTEP 2: Setting up a mapping from the Pore scale to the Averaged REV scale \n";

    // Make a grid for the volume averaged elements
    using AveragedGrid = Dune::YaspGrid<2>;
    DimensionPair numVolumes = getParam<DimensionPair>("VolumeAveraging.NumVolumes");
    std::pair<GlobalPosition, GlobalPosition> averageBox = {getParam<GlobalPosition>("VolumeAveraging.AverageBoxMin"),
                                                            getParam<GlobalPosition>("VolumeAveraging.AverageBoxMax")};
    auto averageGrid = Dune::StructuredGridFactory<AveragedGrid>::createCubeGrid(
                                                   {averageBox.first},
                                                   {averageBox.second},
                                                   {numVolumes[0], numVolumes[1]});
    using AveragedGridGeometry = CCTpfaFVGridGeometry<typename AveragedGrid::LeafGridView>;
    auto averagedGridGeometry = std::make_shared<AveragedGridGeometry>(averageGrid->leafGridView());
    const auto averagedGridView = averageGrid->leafGridView();

    double eps = 1e-12;

    // Set up a vector mapping each Pore Scale element to its averaged element
    std::vector<int> averagedElementIndexMapping;
    averagedElementIndexMapping.resize(poreScaleGridGeometry->elementMapper().size(), -1);

    // Collect the volume of the void space during the mapping to calculate porosity
    std::vector<Scalar> averagedElementVoidSpace;
    averagedElementVoidSpace.resize(averagedGridGeometry->elementMapper().size(), 0.0);

    // Collect the number of sub elements during the mapping to calculate average
    std::vector<int> averagedElementNumSubElements;
    averagedElementNumSubElements.resize(averagedGridGeometry->elementMapper().size(), 0);

    // Fill the mapping vector (and sum up void space)
    for (const auto& poreScaleElement : elements(poreScaleGridView))
    {
        int poreScaleElementIdx = poreScaleGridGeometry->elementMapper().index(poreScaleElement);
        GlobalPosition poreScaleGridElementCenter = poreScaleElement.geometry().center();
        Scalar poreScaleElementVolume = poreScaleElement.geometry().volume();
        for (const auto& averagedElement : elements(averagedGridView))
        {
            int averagedElementIdx = averagedGridGeometry->elementMapper().index(averagedElement);
            auto averagedElementBBMin = averagedElement.geometry().corner(0); // 2D squares only
            auto averagedElementBBMax = averagedElement.geometry().corner(3); // 2D squares only TODO: Enforce

            if ( ( (poreScaleGridElementCenter[0] > averagedElementBBMin[0] + eps)
                && (poreScaleGridElementCenter[0] < averagedElementBBMax[0] - eps) )
              && ( (poreScaleGridElementCenter[1] > averagedElementBBMin[1] + eps)
                && (poreScaleGridElementCenter[1] < averagedElementBBMax[1] - eps) ) )
            {
                averagedElementVoidSpace[averagedElementIdx] += poreScaleElementVolume;
                averagedElementNumSubElements[averagedElementIdx]++;
                averagedElementIndexMapping[poreScaleElementIdx] = averagedElementIdx;
                break;
            }
        }
    }
    std::cout << "Pore Scale to REV scale mapping complete \n";












    //////////////////////////////////////////////
    // Step 3: Calculate Pore Scale Flow Field  //
    //////////////////////////////////////////////

    // run the stationary non-linear problem on the pore scale grid
    std::cout << "\nSTEP 3: Solve the flow problem on the pore scale geometry \n";

    // the problem (initial and boundary conditions)
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    auto problem = std::make_shared<Problem>(poreScaleGridGeometry);

    // the solution vector
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    SolutionVector x;
    x[GridGeometry::cellCenterIdx()].resize(poreScaleGridGeometry->numCellCenterDofs());
    x[GridGeometry::faceIdx()].resize(poreScaleGridGeometry->numFaceDofs());
    problem->applyInitialSolution(x);

    // the grid variables
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    auto gridVariables = std::make_shared<GridVariables>(problem, poreScaleGridGeometry);
    gridVariables->init(x);

    // intialize the vtk output module
    using IOFields = GetPropType<TypeTag, Properties::IOFields>;
    StaggeredVtkOutputModule<GridVariables, SolutionVector> vtkWriter(*gridVariables, x, problem->name());
    vtkWriter.addField(averagedElementIndexMapping, "mapping", Vtk::Precision::int32);
    IOFields::initOutputModule(vtkWriter); // Add model specific output fields
    vtkWriter.write(0.0);

    // the assembler with time loop for instationary problem
    using Assembler = StaggeredFVAssembler<TypeTag, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(problem, poreScaleGridGeometry, gridVariables);

    // the linear solver
    using LinearSolver = Dumux::UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::NewtonSolver<Assembler, LinearSolver>;
    NewtonSolver nonLinearSolver(assembler, linearSolver);

    // linearize & solve
    Dune::Timer timer;
    nonLinearSolver.solve(x);

    // write vtk output
    vtkWriter.write(1.0);

    timer.stop();
    const auto& comm = Dune::MPIHelper::getCollectiveCommunication();
    std::cout << "Solve took " << timer.elapsed() << " seconds on "
            << comm.size() << " processes.\n"
            << "The cumulative CPU time was " << timer.elapsed()*comm.size() << " seconds.\n";


    std::cout << "Flow problem on the pore scale geometry is solved. \n";









    ///////////////////////////////////////////////////////////////////////////
    // Step 4: Volume Average Results and Output to the Volume Averaged Grid //
    ///////////////////////////////////////////////////////////////////////////

    std::cout << "\nSTEP 4: Volume Average Results and Output to the Volume Averaged Grid \n";

    /*
     * Calculate Porosity
    */
    std::vector<Scalar> averagedElementVolume, averagedPorosity;
    averagedPorosity.resize(averagedGridGeometry->elementMapper().size(), 1);
    averagedElementVolume.resize(averagedGridGeometry->elementMapper().size(), 0.0);
    for (const auto& averagedElement : elements(averagedGridView))
    {
        int averagedElementIdx = averagedGridGeometry->elementMapper().index(averagedElement);
        averagedElementVolume[averagedElementIdx] = averagedElement.geometry().volume();
    }
    for (int i = 0; i < averagedGridGeometry->elementMapper().size(); i++)
        averagedPorosity[i] = averagedElementVoidSpace[i] / averagedElementVolume[i];

    std::vector<Scalar> pressure, pressureSum;
    pressure.resize(averagedGridGeometry->elementMapper().size(), 0);
    pressureSum.resize(averagedGridGeometry->elementMapper().size(), 0);
    std::vector <GlobalPosition> velocitySeepage, velocitySum, velocityDarcy;
    velocitySeepage.resize(averagedGridGeometry->elementMapper().size(), GlobalPosition(0.0));
    velocityDarcy.resize(averagedGridGeometry->elementMapper().size(), GlobalPosition(0.0));
    velocitySum.resize(averagedGridGeometry->elementMapper().size(), GlobalPosition(0.0));

    /*
     * Sum all of the averaged fields
    */
    auto poreScalePressure = x[GridGeometry::cellCenterIdx()];
    auto poreScaleVelocity = problem->cellCenteredVelocity(x);
    for (const auto& poreScaleElement : elements(poreScaleGridView))
    {
        int poreScaleElementIdx = poreScaleGridGeometry->elementMapper().index(poreScaleElement);

        // Skip non mapped elements (inlet/outlet)
        if (averagedElementIndexMapping[poreScaleElementIdx] == -1)
            continue;
        int mappedAverageIdx = averagedElementIndexMapping[poreScaleElementIdx];

        pressureSum[mappedAverageIdx] += poreScalePressure[poreScaleElementIdx]*poreScaleElement.geometry().volume();
        for (int j = 0; j <dim; j++)
            velocitySum[mappedAverageIdx][j] += poreScaleVelocity[poreScaleElementIdx][j]*poreScaleElement.geometry().volume();

    }

    // Do Pressure volume averaging
    for (int i = 0; i < averagedGridGeometry->elementMapper().size(); i++)
        pressure[i] = pressureSum[i] / averagedElementVoidSpace[i];

    // Seepage Velocity
    for (int i = 0; i < averagedGridGeometry->elementMapper().size(); i++)
    {
        for (int j = 0; j <dim; j++)
            velocitySeepage[i][j] = velocitySum[i][j] / averagedElementVoidSpace[i];
    }

    // Darcy Velocity volume averaging
    for (int i = 0; i < averagedGridGeometry->elementMapper().size(); i++)
    {
        for (int j = 0; j < dim; j++)
            velocityDarcy[i][j] = velocitySum[i][j] / averagedElementVolume[i];
    }

    /*
     * Write out the averaged fields
    */
    VtkOutputModuleBase<AveragedGridGeometry> averagedVtkWriter(*averagedGridGeometry, "AveragedFields_" + problem->name(), "Double");
    averagedVtkWriter.addField(averagedElementNumSubElements, "numSubElements", Vtk::Precision::float32);
    averagedVtkWriter.addField(averagedPorosity, "porosity", Vtk::Precision::float32);
    averagedVtkWriter.addField(pressure, "pressure", Vtk::Precision::float32);
    averagedVtkWriter.addField(velocitySeepage, "velocity_Seepage", Vtk::Precision::float32);
    averagedVtkWriter.addField(velocityDarcy, "velocity_Darcy", Vtk::Precision::float32);
    averagedVtkWriter.write(0.0);

    std::cout << "Volume averaging completed. \n";








    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;

} // end main
