load "iovtk"
load "MUMPS"
load "gmsh"
load "msh3"
load "tetgen"

real poreLength = 1; // pore length from grid, only used for simulations
real poreLengthReal = 0.05; // dimension in [m]

// the orders for saving to vtk
int[int] orderv=[1, 1, 1];
int[int] orderp=[1];

mesh Th=gmshload("porescalegrid_0.550400_triangular.msh");
func  newlabel = ( x == 0 ) ? 4 : label;
Th=change(Th,flabel= newlabel);
func  newlabel2 = ( x == poreLength ) ? 2 : label;
Th=change(Th,flabel= newlabel2);
func  newlabel3 = ( y == poreLength ) ? 3 : label;
Th=change(Th,flabel= newlabel3);
func  newlabel4 = ( y == 0 ) ? 5 : label;
Th=change(Th,flabel= newlabel4);
plot(Th, wait=true);

int[int] lab = labels(Th);
cout << lab << endl;

func fullperio = [[4,y],[2,y],[3,x],[5,x]];

Th = adaptmesh(Th, hmax = 0.006, nbvx = 2000000, periodic = fullperio,
nbjacoby = 100, nbsmooth = 25);
plot(Th, wait=true);

fespace Ch(Th, [P2,P2,P1], periodic=fullperio);
//fespace Ch(cellmesh, [P1b,P1b,P1], periodic=fullperio);
Ch [wx1, wy1, pc1];
Ch [wxh1, wyh1, pch1];
Ch [wx2, wy2, pc2];
Ch [wxh2, wyh2, pch2];
real a = poreLength;

solve stokes1 ([wx1, wy1, pc1], [wxh1, wyh1, pch1])
    =
    int2d(Th)(
          a*a*dx(wx1)*dx(wxh1)
        + a*a*dy(wx1)*dy(wxh1)
        + a*a*dx(wy1)*dx(wyh1)
        + a*a*dy(wy1)*dy(wyh1)
        + dx(pc1)*wxh1
        + dy(pc1)*wyh1
        + pch1*(dx(wx1) + dy(wy1))
        - 1e-10*pc1*pch1
    )
    + int2d(Th)(-1*wxh1) //
    + on(1, wx1=0, wy1=0)
    ;
real scale = (poreLengthReal*poreLengthReal)/(poreLength*poreLength);
real k11 = int2d(Th)(wx1)*scale;
cout << " k11 = " << k11 << endl << endl;
real k12 = int2d(Th)(wy1)*scale;
cout << " k12 = " << k12 << endl << endl;


solve stokes2 ([wx2, wy2, pc2], [wxh2, wyh2, pch2])
    =
    int2d(Th)(
          a*a*dx(wx2)*dx(wxh2)
        + a*a*dy(wx2)*dy(wxh2)
        + a*a*dx(wy2)*dx(wyh2)
        + a*a*dy(wy2)*dy(wyh2)
        + dx(pc2)*wxh2
        + dy(pc2)*wyh2
        + pch2*(dx(wx2) + dy(wy2))
        - 1e-10*pc2*pch2
    )
    + int2d(Th)(-1*wyh2) //
    + on(1, wx2=0, wy2=0)
    ;
real k21 = int2d(Th)(wx2)*scale;
cout << " k21 = " << k21 << endl << endl;
real k22 = int2d(Th)(wy2)*scale;
cout << " k22 = " << k22 << endl << endl;

savevtk("./out/velocity_1_dumux_"+k11+".vtu",Th,[wx1,wy1,0],dataname="velocity", bin=1, order=orderv);
savevtk("./out/pressure_1_dumux_"+k11+".vtu",Th,pc1,dataname="pressure", bin=1, order=orderp);
// savevtk("./out/velocity_2_dumux.vtu",Th,[wx2,wy2,0],dataname="velocity", bin=1, order=orderv);
// savevtk("./out/pressure_2_dumux.vtu",Th,pc2,dataname="pressure", bin=1, order=orderp);
// =============================================================================
