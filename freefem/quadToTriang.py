import numpy as np
import meshio
import pygmsh


def quad2triang(quads):

    size = quads.shape[0]

    triang = np.zeros([2*size, 3], dtype=int)
    for i in range(size):
        triang[2*i,:] = quads[i, 0:3]
        triang[2*i+1,:] = np.array([quads[i, 0], quads[i, 2], quads[i,3]], dtype=int)

    return triang




if __name__ == '__main__':

    quads =  np.array([[1,2,5,6], [2,3,4,5]], dtype = int)
    nodes = np.array([[0.0 , 0.0], [0.5, 0], [1.0, 0.0], [1.0, 0.5], [0.5, 0.5], [0.0 , 0.5]])

    triang = quad2triang(quads)

#pygmsh.read("porescalegrid.msh")
#meshio.read("porescalegrid.msh")

datei = open('porescalegrid_0.550400.txt','r')
#new_datei = open('new','w')

lines_datei = datei.readlines()
for i in range(0,len(lines_datei)):
    if '$Elements' in lines_datei[i]:
            index = i

with open('porescalegrid_0.550400_triangular.txt', 'w') as temp_file:
    for item in lines_datei[0:index+1]:
        temp_file.write(item)

file = open('porescalegrid_0.550400_triangular.txt', 'a')

last_line = lines_datei[-1]

elements = lines_datei[index+2:-1]
file.write(str(2*len(elements)))
file.write("\n")

new_elements = np.zeros((2*len(elements),6))
for i in range(0,len(elements)):
    element = elements[i].split(' ')
    new_elements[2*i,:] = [2*(i+1)-1,2,0,int(element[3]),int(element[4]),int(element[5])]
    new_elements[2*i+1,:] = [2*(i+1),2,0,int(element[3]),int(element[5]),int(element[6])]

np.savetxt('new_elements.txt',new_elements.astype(int), fmt = '%i', delimiter = " ")

new_elements = open('new_elements.txt','r')
lines_datei_new = new_elements.readlines()
for item in lines_datei_new:
    file.write(item)
file.write(last_line)
file.close()
